% Eliminasi Berdasarkan Luas Dan Bentuk
blobMeasurements = regionprops(labeledCandidate, 'Area', 'Eccentricity');

allArea = [blobMeasurements.Area];
allEccentricity = [blobMeasurements.Eccentricity];

meanArea = mean(allArea);
stdArea = std(allArea);

indexBlob = find(allArea >= 24 & allArea <= (meanArea + stdArea) & allEccentricity < 0.81);
ambilBlob = ismember(labeledCandidate, indexBlob);

blobBW = ambilBlob > 0;
[labeledBlob, numberOfBlobs] = bwlabel(blobBW);

figure, imshow(blobBW);