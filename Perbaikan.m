% Sharpening
wajahG = rgb2gray(wajah);
h = fspecial('log', [9 9], 2);
m = imfilter(wajahG, h, 'circular', 'same', 'conv');

% Labeling
candidate = logical(m);
[labeledCandidate, numberOfCandidates] = bwlabel(candidate, 8);

figure, imshow(wajahG);
figure, imshow(candidate);