citra = imread('bahan-uts.jpg');

% Ukuran Citra Semula
[bar, kol, dlm] = size(citra);

if (bar > kol)
    maxLength = bar;
    if (maxLength >= 480)
        citra = imresize(citra, [480 NaN]);
    end
else
    maxLength = kol;
    if (maxLength >= 480)
        citra = imresize(citra, [NaN 480]);
    end
end