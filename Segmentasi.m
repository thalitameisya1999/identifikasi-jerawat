citraG = rgb2gray(citra);
level = graythresh(citraG);

citraB = imbinarize(citraG, level);
citraB = imfill(citraB, 'holes');

citraCC = bwconncomp(citraB);
citraL = labelmatrix(citraCC);

% Ukuran Citra Setelah Resize
[bar, kol, dlm] = size(citra);

% Cari Connected Component Terbanyak
terluas = 0;
for i = 1 : length(citraCC.PixelIdxList)
    if (length(citraCC.PixelIdxList{i})) > terluas
        terluas = length(citraCC.PixelIdxList{i});
        index = i;
    end
end

% Ambil Label
wajah = uint8(zeros(bar, kol, dlm));
for i = 1 : bar
    for j = 1 : kol
        if citraL(i, j) == index
            wajah(i, j, :) = citra(i, j, :);
        end
    end
end

figure, imshow(citra);
figure, imshow(citraG);
figure, imshow(citraB);
figure, imshow(wajah);