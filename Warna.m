% Eliminasi Berdasarkan Warna
red = citra(:, :, 1);
green = citra(:, :, 2);
blue = citra(:, :, 3);

r = regionprops(labeledBlob, red, 'MeanIntensity');
g = regionprops(labeledBlob, green, 'MeanIntensity');
b = regionprops(labeledBlob, blue, 'MeanIntensity');

fiturR = [r.MeanIntensity]';
fiturG = [g.MeanIntensity]';
fiturB = [b.MeanIntensity]';
fitur = [fiturR fiturG fiturB];

meanR = mean(fiturR);
meanG = mean(fiturG);
meanB = mean(fiturB);
stdR = std(fiturR);
stdG = std(fiturG);
stdB = std(fiturB);

indexJerawat = [];
for i = 1 : numberOfBlobs
    if(fiturR(i) >= (meanR-stdR*1.75) && fiturR(i) <= (meanR+stdR*1.75) && fiturG(i) >= (meanG-stdG*1.75) && fiturG(i) <= (meanG+stdG*1.75) && fiturB(i) >= (meanB-stdB*1.75) && fiturB(i) <= (meanB+stdB*1.75))
        indexJerawat = [indexJerawat i];
    end
end
jumlahJerawat = length(index);
jerawatBW = ismember(labeledBlob, indexJerawat);

figure, imshow(jerawatBW);